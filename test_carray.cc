#include <valarray>
#include <iostream> // std::cout, std::endl
#include <iomanip>  // std::setw
#include <fstream>  // std::ofstream
template <typename T>
class CArray : public std::valarray<T> {
  size_t n1,n2,n3;
public:
  CArray(size_t nr, size_t ng=1, size_t nb=1) : std::valarray<T>(nr*ng*nb) {
    n1=nr,n2=ng,n3=nb;
  }
  CArray(const CArray<T>&) = delete; //copy constructor
	CArray& operator()(const CArray<T>&) = delete; //copy assignment operator
  T operator()(size_t ix, size_t iy=0, size_t iz=0) const {
    return (*this)[iz*n2*n1+iy*n1+ix]; 
  }//read Array
  T& operator()(size_t ix, size_t iy=0, size_t iz=0) {
    return (*this)[iz*n2*n1+iy*n1+ix]; 
  }//write Array
  size_t Dim(size_t n) const {return (n>2)? n3 : ((n<2)? n1 : n2);}
};

void show (const CArray<float> &A);
void writeRawData( char* name, const CArray<float> &data);

int main(){
  const size_t nz=11;
  const size_t ny=9;
  const size_t nx=6;

  CArray<float> a(nz);       //columns
  CArray<float> b(nz,ny);    //columns x rows 
  CArray<float> c(nz,ny,nx); //columns x rows x slices
  
  for ( size_t i=0; i<a.Dim(1); i++ )//by columns
    a[i] = (float)i;
  
  show(a);
  std::cout << "----------------------------" << std::endl;

  for ( size_t j=0; j<b.Dim(2); j++ )//by rows
    for ( size_t i=0; i<b.Dim(1); i++ )//by columns
      b(i,j) = j*nz+i;
      ///columns=i,rows=j
  
  show(b);
  std::cout << "----------------------------" << std::endl;
  
  for ( size_t k=0; k<c.Dim(3); k++ )//by slices
    for ( size_t j=0; j<c.Dim(2); j++ )//by rows
      for ( size_t i=0; i<c.Dim(1); i++ )//by columns
        c(i,j,k) = k*ny*nz + j*nz + i;
        ///columns=i,rows=j,slices=k
  
  show(c);

  for ( size_t k=0; k<c.Dim(3); k++ ) {//by slices
    for ( size_t j=0; j<c.Dim(2); j++ ) {//by rows
      for ( size_t i=0; i<c.Dim(1); i++ ) {//by columns
        float z =  (i*0.5-2.0);
        float y =  (j*0.5-1.0);
        float x =  (k*0.5-2.0);
        c(i,j,k) = z*z + y*y + x*x;
      }
    }
  }
  // export to binary data
  writeRawData((char*)"dat3d.raw",c);
}

void show (const CArray<float> &A) {
  for ( size_t i=0; i<A.Dim(3); i++ ) {
    for ( size_t j=0; j<A.Dim(2); j++ ) {
      for ( size_t k=0; k<A.Dim(1); k++ ) {
        std::cout << std::setw(4) << A(k,j,i);
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}

void writeRawData( char* name, const CArray<float> &data){
    std::ofstream outfile; 
    outfile.open(name, std::ios::out | std::ios::binary);  
    outfile.write( (char*) &data[0], data.size()*sizeof(float));
    outfile.close();
}
