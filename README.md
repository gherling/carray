# CArray

Definition of an optimal array type class for use in 2D and 3D finite difference numerical computation. It must take into account the access position of the array through columns, rows and slice. It is designed for optimal performance in C++ when the compiler is not allowed to choose a copy constructor and a copy assignment operator by default. 

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gherling/carray.git
git branch -M main
git push -uf origin main
```
## License

This work is licensed under the GNU General Public License (GPL) v3.0. See the
`LICENSE` file for more details.

## Author

You can contact me to email adress:
* Herling Gonzalez Alvarez: <gherling@gmail.com>
